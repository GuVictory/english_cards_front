// import React
import React, { useState } from 'react';
import { Route, Switch, Redirect, withRouter } from 'react-router-dom';

import './scss/App.scss';

import Header from './components/Header';
import PlayGround from './pages/PlayGround';
import Login from './pages/Login';
import Register from './pages/Register';
import EngAdmin from './pages/EngAdmin';
import axios_conf from './axios-config';

const App = ({ history }) => {
    const [authTocken, setAuthTocken] = useState(null);
    const [isEngTutor, setEngTutor] = useState(false);
    const [userId, setUserId] = useState(false);

    async function logoutUser() {
        const data = { user_id: userId };

        axios_conf
            .post('users/logout', data)
            .then(() => {
                setAuthTocken(null);
                console.log('User was logout!');
            })
            .catch((err) => {
                console.error('Logout error', err);
            });
    }

    return (
        <div className='p-grid p-dir-col' id='app'>
            <Header
                authTocken={authTocken}
                isEngTutor={isEngTutor}
                logoutUser={logoutUser}
            />
            <div className='p-col'>
                <Switch>
                    <Route exact path='/'>
                        {authTocken ? (
                            <Redirect to='/playground' />
                        ) : (
                            <Redirect to='/login' />
                        )}
                    </Route>

                    <Route exact path='/login'>
                        {authTocken ? (
                            <Redirect to='/playground' />
                        ) : (
                            <Login
                                setAuthTocken={setAuthTocken}
                                setEngTutor={setEngTutor}
                                setUserId={setUserId}
                            />
                        )}
                    </Route>

                    <Route exact path='/register'>
                        {authTocken ? (
                            <Redirect to='/playground' />
                        ) : (
                            <Register
                                setAuthTocken={setAuthTocken}
                                setUserId={setUserId}
                            />
                        )}
                    </Route>

                    <Route exact path='/logout'>
                        {authTocken ? (
                            <Redirect to='/playground' />
                        ) : (
                            <Redirect to='/login' />
                        )}
                    </Route>

                    <Route exact path='/playground'>
                        {authTocken ? (
                            <PlayGround authTocken={authTocken} />
                        ) : (
                            <Redirect to='/login' />
                        )}
                    </Route>
                </Switch>
            </div>
        </div>
    );
};

export default withRouter(App);

/***
 * <Route exact path='/playground'>
                        {authTocken ? <PlayGround /> : <Redirect to='/login' />}
                    </Route>
 */
