import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import { Button } from 'primereact/button';
import Logo from '../assets/images/logo.png';

import { Menubar } from 'primereact/menubar';

const Header = ({ authTocken, isEngTutor, logoutUser }) => {
    const TutorBtn = () => (
        <Fragment>
            <Link to='/eng_admin'>
                <Button
                    label='Панель управления'
                    className='p-button-raised p-mr-2'
                />
            </Link>
            <Link to='/logout'>
                <Button
                    label='Выход'
                    className='p-button-outlined p-button-danger'
                    onClick={() => logoutUser()}
                />
            </Link>
        </Fragment>
    );

    const StudentBtn = () => (
        <Fragment>
            <Link to='/logout'>
                <Button
                    label='Выход'
                    className='p-button-outlined p-button-danger'
                    onClick={() => logoutUser()}
                />
            </Link>
        </Fragment>
    );

    const start = (
        <img
            alt='logo'
            src={Logo}
            onError={(e) =>
                (e.target.src =
                    'https://www.primefaces.org/wp-content/uploads/2020/05/placeholder.png')
            }
            height='40'
            className='p-mr-6'></img>
    );
    const end = !authTocken ? (
        <Fragment>
            <Link to='/login'>
                <Button label='Вход' className='p-button-outlined p-mr-2' />
            </Link>
            <Link to='/register'>
                <Button label='Регистрация' className='p-button-raised' />
            </Link>
        </Fragment>
    ) : isEngTutor ? (
        <TutorBtn />
    ) : (
        <StudentBtn />
    );

    const menuItems = [
        {
            label: 'Учиться',
        },
        {
            label: 'Курсы',
        },
        {
            label: 'Стать учителем',
        },
        {
            label: 'Команда',
        },
        {
            label: 'Английский язык',
        },
    ];

    return (
        <div className='card p-col-fixed' style={{ height: '105px' }}>
            <Menubar model={menuItems} start={start} end={end} />
        </div>
    );
};

export default Header;
