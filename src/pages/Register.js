import React, { useState, Fragment } from 'react';
import { InputText } from 'primereact/inputtext';
import { Password } from 'primereact/password';
import { Card } from 'primereact/card';
import { Button } from 'primereact/button';
import { Messages } from 'primereact/messages';
import axios_conf from '../axios-config';

const Register = ({ setAuthTocken, setUserId }) => {
    const [email, setEmail] = useState('');
    const [password, setPasword] = useState('');
    const [passwordRep, setPaswordRep] = useState('');
    const [errorMsgs, setErrorMsgs] = useState(null);

    async function registerUser() {
        const data = { email: email, password: password };

        if (email === '' || password === '' || passwordRep === '') {
            errorMsgs.show([
                {
                    severity: 'error',
                    detail: 'Заполните все поля!',
                    sticky: true,
                },
            ]);
            return;
        }

        if (password !== passwordRep) {
            errorMsgs.show([
                {
                    severity: 'error',
                    detail: 'Пароли не совпадают!',
                    sticky: true,
                },
            ]);
            return;
        }

        axios_conf
            .post(`users/register`, data)
            .then((res) => {
                console.log('Успех:', res.data);

                setAuthTocken(`Token ${res.data.auth_token}`);
                setUserId(res.data.id);
            })
            .catch((err) => {
                errorMsgs.show([
                    {
                        severity: 'error',
                        detail: 'Пользователь с таким email уже есть!',
                        sticky: true,
                    },
                ]);
            });
    }

    return (
        <div className='card p-d-flex p-flex-column p-jc-center p-ai-center'>
            <Card
                title='Регистрация'
                className='p-p-5'
                style={{ minWidth: '400px' }}>
                <div className='p-d-flex p-flex-column'>
                    <span className='p-mb-2 p-d-inline-flex'>Email</span>
                    <InputText
                        className='p-d-inline-flex p-mb-4'
                        id='email'
                        type='email'
                        value={email}
                        placeholder='Email'
                        onChange={(e) => setEmail(e.target.value)}
                    />

                    <span className='p-mb-2 p-d-inline-flex'>Пароль</span>
                    <Password
                        className='p-d-inline-flex p-mb-4'
                        value={password}
                        onChange={(e) => setPasword(e.target.value)}
                        placeholder='Пароль'
                    />

                    <span className='p-mb-2 p-d-inline-flex'>
                        Повтор пароля
                    </span>
                    <InputText
                        className='p-d-inline-flex p-mb-4'
                        type='password'
                        value={passwordRep}
                        placeholder='Пароль'
                        onChange={(e) => setPaswordRep(e.target.value)}
                    />

                    <Button
                        label='Зарегистрироваться'
                        className='p-button-raised p-mr-2'
                        type='button'
                        onClick={() => registerUser()}
                    />
                </div>
            </Card>
            <Messages
                ref={(el) => setErrorMsgs(el)}
                style={{ minWidth: '400px' }}
            />
        </div>
    );
};

export default Register;
