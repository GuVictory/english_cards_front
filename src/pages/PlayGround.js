import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

import { Carousel } from 'primereact/carousel';
import { Button } from 'primereact/button';
import { useSpring, animated as a } from 'react-spring';
import axios_conf from '../axios-config';

import '../scss/pages/PlayGround.scss';

const PlayGround = ({ authTocken }) => {
    const [flipped, setFlipped] = useState(false);
    const [cards, setCards] = useState([]);

    const { transform, opacity } = useSpring({
        opacity: flipped ? 1 : 0,
        transform: `perspective(600px) rotateX(${flipped ? 180 : 0}deg)`,
        config: { mass: 5, tension: 500, friction: 80 },
    });

    useEffect(() => {
        const headers = { headers: { Authorization: authTocken } };
        axios_conf.get('eng', headers).then((res) => {
            console.log(res.data);
            setCards(res.data);
        });
    }, []);

    const cardTemplate = (card) => {
        return (
            <div
                className='p-d-flex p-flex-column p-jc-center p-ai-center'
                onClick={() => setFlipped((state) => !state)}>
                <a.div
                    className='c back p-d-flex p-jc-center p-ai-center p-flex-column'
                    style={{
                        opacity: opacity.interpolate((o) => 1 - o),
                        transform,
                    }}>
                    <div className='p-text-normal c__text'>
                        {card.in_english}
                    </div>
                </a.div>
                <a.div
                    className='c front p-d-flex p-jc-center p-ai-center p-flex-column'
                    style={{
                        opacity,
                        transform: transform.interpolate(
                            (t) => `${t} rotateX(180deg)`,
                        ),
                    }}>
                    <div className='p-text-normal c__text'>
                        {card.in_russian}
                    </div>
                </a.div>
            </div>
        );
    };

    return (
        <div
            className='p-d-flex p-flex-column p-jc-center p-ai-center carousel'
            style={{ minWidth: '600px' }}>
            <div className='p-d-flex p-ai-center p-jc-center carousel'>
                <Carousel
                    style={{ minWidth: '600px', minHeight: '300px' }}
                    value={cards}
                    containerClassName='carousel__content'
                    numVisible={1}
                    numScroll={1}
                    itemTemplate={cardTemplate}
                    header={<h5>Карточки-термины</h5>}
                />
            </div>
        </div>
    );
};

export default PlayGround;
