const axios = require('axios').default;

// Set config defaults when creating the instance
const axios_conf = axios.create({
    baseURL: 'http://localhost:8000/api/',
});
// Access-Control-Allow-Origin
axios_conf.defaults.headers.post['Content-Type'] = 'application/json';

export default axios_conf;
