import '@babel/polyfill';
import React, { useState, Fragment } from 'react';
import { InputText } from 'primereact/inputtext';
import { Password } from 'primereact/password';
import { Card } from 'primereact/card';
import { Button } from 'primereact/button';
import { Messages } from 'primereact/messages';
import axios_conf from '../axios-config';

const Login = ({ setAuthTocken, setEngTutor, setUserId }) => {
    const [email, setEmail] = useState('');
    const [password, setPasword] = useState('');
    const [errorMsgs, setErrorMsgs] = useState(null);

    async function loginUser() {
        const data = { email: email, password: password };

        if (email === '' || password === '') {
            errorMsgs.show([
                {
                    severity: 'error',
                    detail: 'Заполните все поля!',
                    sticky: true,
                },
            ]);

            return;
        }

        axios_conf
            .post(`users/login`, data)
            .then((res) => {
                console.log('Успех:', res.data);
                setAuthTocken(`Token ${res.data.auth_token}`);
                setEngTutor(res.data.is_eng_tutor);
                setUserId(res.data.id);
            })
            .catch((err) => {
                errorMsgs.show([
                    {
                        severity: 'error',
                        detail: 'Данные не верные!',
                        sticky: true,
                    },
                ]);
            });
    }

    return (
        <div className='card p-d-flex p-flex-column p-jc-center p-ai-center'>
            <Card title='Вход' className='p-p-5' style={{ minWidth: '400px' }}>
                <div className='p-d-flex p-flex-column'>
                    <span className='p-mb-2 p-d-inline-flex'>Email</span>
                    <InputText
                        className='p-d-inline-flex p-mb-4'
                        id='email'
                        type='email'
                        value={email}
                        placeholder='Email'
                        onChange={(e) => setEmail(e.target.value)}
                    />

                    <span className='p-mb-2 p-d-inline-flex'>Пароль</span>
                    <Password
                        className='p-d-inline-flex p-mb-4'
                        value={password}
                        onChange={(e) => setPasword(e.target.value)}
                        placeholder='Пароль'
                    />

                    <Button
                        label='Войти'
                        className='p-button-raised p-mr-2'
                        type='button'
                        onClick={() => loginUser()}
                    />
                </div>
            </Card>
            <Messages
                ref={(el) => setErrorMsgs(el)}
                style={{ minWidth: '400px' }}
            />
        </div>
    );
};

export default Login;
